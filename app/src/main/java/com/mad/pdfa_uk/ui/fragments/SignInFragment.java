package com.mad.pdfa_uk.ui.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mad.pdfa_uk.R;
import com.mad.pdfa_uk.databinding.FragmentSigninBinding;
import com.mad.pdfa_uk.modals.SignInViewModal;
import com.mad.pdfa_uk.utils.AppListeners;
import com.mad.pdfa_uk.utils.AppUtils;

/**
 * Created By Madhur on 10/8/18 , 10:48 AM
 */
public class SignInFragment extends Fragment implements AppListeners.LoginClickHandler{

    private View rootView;
    private Context ctx;
    private FragmentSigninBinding signinBinding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        signinBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_signin,container,false);
        rootView = signinBinding.getRoot();

        iniView(rootView);

        return rootView;
    }

    private void iniView(View rootView) {
        signinBinding.setLoginDetails(new SignInViewModal());
        signinBinding.setHandler(this);
    }

    @Override
    public void onLoginClick(View view) {

        if (AppUtils.isOnline(getActivity())){
            if (isValid()){
                //TODO Login
                Toast.makeText(getActivity(), "Data : " + signinBinding.getLoginDetails().email + signinBinding.getLoginDetails().password, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onForgotPassword(View view) {

    }

    @Override
    public void onRegistrationClick(View view) {
        AppUtils.setFragment(R.id.container,getFragmentManager(),new SignUpFragment(),true);
    }

    private boolean isValid() {
        if (AppUtils.isValidEmail(signinBinding.getLoginDetails().email.get())){
            signinBinding.etEmailSignIn.setError(getString(R.string.please_fill_valid) + " " +getString(R.string.email));
            return false;
        }
        else if (AppUtils.isValidPassword(signinBinding.getLoginDetails().password.get())){
            signinBinding.etEmailSignIn.setError(getString(R.string.please_fill_valid_password) + " " +getString(R.string.password));
            return false;
        }
        return true;
    }
}
