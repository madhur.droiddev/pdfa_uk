package com.mad.pdfa_uk.ui.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.FrameLayout;

import com.mad.pdfa_uk.R;
import com.mad.pdfa_uk.ui.fragments.SignInFragment;
import com.mad.pdfa_uk.utils.AppUtils;

public class MainActivity extends BaseActivity {

    FrameLayout frameLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        frameLayout = findViewById(R.id.snackBarView);

        AppUtils.setFragment(R.id.container,getSupportFragmentManager(),new SignInFragment(),false);
    }

    public void showSnackBar(View view, String msg){
        Snackbar snackbar = Snackbar
                .make(frameLayout, msg, Snackbar.LENGTH_LONG);
        snackbar.show();
    }
}
