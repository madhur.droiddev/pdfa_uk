package com.mad.pdfa_uk.ui.fragments;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import com.mad.pdfa_uk.databinding.FragmentSignupBinding;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mad.pdfa_uk.R;
import com.mad.pdfa_uk.modals.RegistrationResponseModal;
import com.mad.pdfa_uk.modals.SignUpViewModal;
import com.mad.pdfa_uk.retrofit.ApiClient;
import com.mad.pdfa_uk.retrofit.ApiInterface;
import com.mad.pdfa_uk.ui.activities.MainActivity;
import com.mad.pdfa_uk.utils.AppListeners;
import com.mad.pdfa_uk.utils.AppUtils;
import com.mad.pdfa_uk.utils.LoadingDialog;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created By Madhur on 12/9/18 , 10:19 AM
 */
public class SignUpFragment extends Fragment implements AppListeners.RegisterClickHandler {

    private View rootView;
    private FragmentSignupBinding signupBinding;
    private SignUpViewModal signUpViewModal;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        signupBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_signup,container,false);
        rootView = (signupBinding).getRoot();

        initBinding(signupBinding);

        return rootView;
    }

    private void initBinding(FragmentSignupBinding signupBinding) {
        signUpViewModal = new SignUpViewModal();
        signupBinding.setHandlerClick(this);
        signupBinding.setSignUpViewModal(signUpViewModal);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onRegisterClick(View view) {
        if (isValid()){
            int index = signUpViewModal.email.get().indexOf('@');
            String userName = signUpViewModal.email.get().substring(0,index);
            signUpViewModal.username.set(userName);

            signUp(signUpViewModal,new LoadingDialog(getActivity()));

        }else {
            ((MainActivity)Objects.requireNonNull(getActivity())).showSnackBar(view,"Please see the errors");
        }
    }

    private void signUp(SignUpViewModal signUpViewModal, final LoadingDialog loadingDialog) {
        loadingDialog.showDialog("Please Wait");
        ApiClient.getClient(getActivity()).create(ApiInterface.class).signUp(signUpViewModal.username.get(),
                signUpViewModal.name.get(),
                signUpViewModal.password.get(),
                signUpViewModal.email.get(),
                signUpViewModal.lastName.get())
                .enqueue(new Callback<RegistrationResponseModal>() {
            @Override
            public void onResponse(Call<RegistrationResponseModal> call, Response<RegistrationResponseModal> response) {
                loadingDialog.hideDialog();
                if (response.body() != null){
                    RegistrationResponseModal registrationResponseModal = response.body();
                    if (registrationResponseModal.getSuccess()){
                        Toast.makeText(getActivity(), "Register Successfully.", Toast.LENGTH_SHORT).show();
                        redirectToMainScreen();
                    }else {
                        ((MainActivity)getActivity()).showSnackBar(null,registrationResponseModal.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<RegistrationResponseModal> call, Throwable t) {
                loadingDialog.hideDialog();
            }
        });
    }

    private void redirectToMainScreen() {
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onAlreadyMemberClick(View view) {
        Objects.requireNonNull(getActivity()).onBackPressed();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClose(View view) {
        Objects.requireNonNull(getActivity()).onBackPressed();
    }

    private boolean isValid() {
        if (!AppUtils.isValidEmail(String.valueOf(signupBinding.getSignUpViewModal().email.get()))){
            signupBinding.etEmailSignUp.setError(getActivity().getString(R.string.please_fill_valid));
            return false;
        }else if (!AppUtils.isValidPassword(signupBinding.getSignUpViewModal().password.get())){
            signupBinding.etPasswordSignUp.setError(getActivity().getString(R.string.please_fill_valid_password));
            return false;
        }else if (TextUtils.isEmpty(signupBinding.etNameSignUp.getText())){
            signupBinding.etNameSignUp.setError(getString(R.string.please_fill_filed));
        }
        return true;
    }
}
