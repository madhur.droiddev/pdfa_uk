package com.mad.pdfa_uk.utils;

import android.databinding.Observable;
import android.databinding.ObservableField;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.Editable;
import android.text.TextWatcher;

import java.util.Objects;

/**
 * Created By Madhur on 10/8/18 , 11:20 AM
 */
public class TextWatcherAdapter implements TextWatcher {

    public final ObservableField<String> value =
            new ObservableField<>();
    private final ObservableField<String> field;

    private boolean isInEditMode = false;

    public TextWatcherAdapter(final ObservableField<String> field) {
        this.field = field;

        field.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback(){
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                if (isInEditMode){
                    return;
                }
                value.set(field.get());
            }
        });
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void afterTextChanged(Editable editable) {
        if (!Objects.equals(field.get(), editable.toString())) {
            isInEditMode = true;
            field.set(editable.toString());
            isInEditMode = false;
        }
    }
}
