package com.mad.pdfa_uk.utils

import android.view.View

/**
 * Created By Madhur on 10/8/18 , 11:35 AM
 */
class AppListeners {

    interface LoginClickHandler {
        fun onLoginClick(view: View)
        fun onForgotPassword(view: View)
        fun onRegistrationClick(view: View)
    }

    interface RegisterClickHandler {
        fun onRegisterClick(view: View)
        fun onAlreadyMemberClick(view: View)
        fun onClose(view: View)
    }

}
