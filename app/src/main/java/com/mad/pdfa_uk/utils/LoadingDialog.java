package com.mad.pdfa_uk.utils;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created By Madhur on 29/10/18 , 5:22 PM
 */
public class LoadingDialog {
    private ProgressDialog progressDialog;
    private static LoadingDialog INSTANCE = new LoadingDialog();



    public LoadingDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
    }

    public LoadingDialog() {

    }

    public void showDialog(String message) {
        if (!progressDialog.isShowing()) {
            progressDialog.setMessage(message);
            progressDialog.show();
        }
    }

    public void hideDialog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public static LoadingDialog getLoadingDialog() {
        return INSTANCE;
    }
}
