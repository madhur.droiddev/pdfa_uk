package com.mad.pdfa_uk.modals;

import android.databinding.ObservableField;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mad.pdfa_uk.utils.TextWatcherAdapter;

/**
 * Created By Madhur on 29/10/18 , 1:02 PM
 */
public class SignUpViewModal {
    @SerializedName("email")
    @Expose
    public final ObservableField<String> email =
            new ObservableField<>();
    @SerializedName("password")
    @Expose
    public final ObservableField<String> password =
            new ObservableField<>();
    @SerializedName("name")
    @Expose
    public final ObservableField<String> name =
            new ObservableField<>();
    @SerializedName("lastName")
    @Expose
    public final ObservableField<String> lastName =
            new ObservableField<>();
    @SerializedName("username")
    @Expose
    public final ObservableField<String> username =
            new ObservableField<>();


    public TextWatcherAdapter emailWatcher = new TextWatcherAdapter(email);
    public TextWatcherAdapter passwordWatcher = new TextWatcherAdapter(password);
    public TextWatcherAdapter userNameWatcher = new TextWatcherAdapter(username);
    public TextWatcherAdapter nameWatcher = new TextWatcherAdapter(name);
    public TextWatcherAdapter lastNameWatcher = new TextWatcherAdapter(lastName);
}
