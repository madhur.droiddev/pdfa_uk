package com.mad.pdfa_uk.modals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created By Madhur on 29/10/18 , 5:20 PM
 */
public class RegistrationResponseModal {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("user")
        @Expose
        private User user;
        @SerializedName("token")
        @Expose
        private String token;

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
        public class User {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("username")
            @Expose
            private String username;
            @SerializedName("email")
            @Expose
            private String email;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("lastName")
            @Expose
            private String lastName;
            @SerializedName("token")
            @Expose
            private String token;
            @SerializedName("token_updateTime")
            @Expose
            private Integer tokenUpdateTime;
            @SerializedName("active")
            @Expose
            private Integer active;
            @SerializedName("userDevice")
            @Expose
            private Object userDevice;
            @SerializedName("admin")
            @Expose
            private Boolean admin;
            @SerializedName("schoolAdmin")
            @Expose
            private Boolean schoolAdmin;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getToken() {
                return token;
            }

            public void setToken(String token) {
                this.token = token;
            }

            public Integer getTokenUpdateTime() {
                return tokenUpdateTime;
            }

            public void setTokenUpdateTime(Integer tokenUpdateTime) {
                this.tokenUpdateTime = tokenUpdateTime;
            }

            public Integer getActive() {
                return active;
            }

            public void setActive(Integer active) {
                this.active = active;
            }

            public Object getUserDevice() {
                return userDevice;
            }

            public void setUserDevice(Object userDevice) {
                this.userDevice = userDevice;
            }

            public Boolean getAdmin() {
                return admin;
            }

            public void setAdmin(Boolean admin) {
                this.admin = admin;
            }

            public Boolean getSchoolAdmin() {
                return schoolAdmin;
            }

            public void setSchoolAdmin(Boolean schoolAdmin) {
                this.schoolAdmin = schoolAdmin;
            }

        }
    }
}
