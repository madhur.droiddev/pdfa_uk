package com.mad.pdfa_uk.modals;

import android.databinding.ObservableField;
import com.mad.pdfa_uk.utils.TextWatcherAdapter;

/**
 * Created By Madhur on 10/8/18 , 10:52 AM
 */
public class SignInViewModal {
    public final ObservableField<String> email =
            new ObservableField<>();
    public final ObservableField<String> password =
            new ObservableField<>();

    public TextWatcherAdapter emailWatcher = new TextWatcherAdapter(email);
    public TextWatcherAdapter passwordWatcher = new TextWatcherAdapter(password);


}
