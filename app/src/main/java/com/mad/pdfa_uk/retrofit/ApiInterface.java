package com.mad.pdfa_uk.retrofit;

import com.mad.pdfa_uk.modals.RegistrationResponseModal;
import com.mad.pdfa_uk.modals.SignInViewModal;
import com.mad.pdfa_uk.modals.SignUpViewModal;
import com.mad.pdfa_uk.utils.ApiLink;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created By Madhur on 29/10/18 , 12:51 PM
 */
public interface ApiInterface {


    @FormUrlEncoded
    @POST(ApiLink.SIGNUP)
    Call<RegistrationResponseModal> signUp(@Field("userName") String userName,
                                           @Field("name") String name,
                                           @Field("password") String password,
                                           @Field("email") String userType,
                                           @Field("lastName") String lastName);
}
