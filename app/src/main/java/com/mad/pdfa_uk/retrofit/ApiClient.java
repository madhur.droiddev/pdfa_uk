package com.mad.pdfa_uk.retrofit;

import android.content.Context;
import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created By Madhur on 12/10/18 , 2:48 PM
 */
public class ApiClient {

    public static final String BASE_URL = "https://boiling-atoll-18288.herokuapp.com";

    private static Retrofit retrofit = null;

    public static Retrofit getClient(final Context context) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(logging);
        /*httpClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder();
                *//*Log.d("HTTP_AUTH_TOKEN", "token : " + AppSharedPref.getAuthToken(context));*//*
                *//*if (AppSharedPref.getAuthToken(context) != null) {
                    requestBuilder.addHeader("Authorization", "Token " + AppSharedPref.getAuthToken(context));
                }*//*
                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });*/

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
